$(document).ready(function() {
    $('.testmonial').owlCarousel({
        loop: false,
        responsiveClass: true,
        items: 3,
        margin: 20,
        nav: true,
        dots: false,
        navText: ["<img src='assets/images/back.png'>", "<img src='assets/images/next.png'>"],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 3,
            }
        }
    })
});